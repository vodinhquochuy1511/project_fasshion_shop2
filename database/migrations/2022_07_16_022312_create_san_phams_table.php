<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::create('san_phams', function (Blueprint $table) {
            $table->id();
            $table->string('ma_san_pham');
            $table->string('ten_san_pham');
            $table->string('slug_san_pham');
            $table->integer('gia_ban');
            $table->integer('phan_tram_khuyen_mai')->nullable();
            $table->integer('gia_khuyen_mai');
            $table->integer('so_luong');
            $table->string('hinh_anh');
            $table->integer('id_danh_muc');
            $table->integer('is_open');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('san_phams');
    }
};
