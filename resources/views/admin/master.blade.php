<!DOCTYPE html>
<html lang="en">
 @include('admin.share.head')
  <body onload="startTime()">
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
      <div class="page-header">
       @include('admin.share.top')
      </div>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        @include('admin.share.menu')
        <!-- Page Sidebar Ends-->
        @yield('content')
        <!-- footer start-->
        @include('admin.share.foot')
      </div>
    </div>
    <!-- latest jquery-->
    @include('admin.share.js')
    @yield('js')
  </body>
</html>
