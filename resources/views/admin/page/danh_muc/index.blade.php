@extends('admin.master')
@section('content')
<div id="app" class="page-body">
    <div class="container-fuild">
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                       <h3> Thêm mới danh mục quần áo</h3>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                              <label>Mã Danh Mục</label>
                              <input type="text" v-model="add.ma_danh_muc" name="ma_danh_muc" class="form-control">
                            </div>
                            <div class="form-group mt-2">
                              <label>Tên Danh Mục</label>
                              <input type="text" v-on:keyup="converToSlug(add.ten_danh_muc)" v-model="add.ten_danh_muc" name="ten_danh_muc" class="form-control">
                            </div>
                            <div class="form-group mt-2">
                                <label>Slug Danh Mục</label>
                                <input type="text" v-model="add.slug_danh_muc" class="form-control">
                            </div>
                            <div class="form-group mt-2">
                                <label>Danh Mục Cha</label>
                                <select class="form-control" name="id_danh_muc_cha" v-model="add.id_danh_muc_cha">
                                    <option value="0">Root</option>
                                    <option v-for="(value, key) in listDanhMucCha" v-bind:value="value.id">@{{ value.ten_danh_muc}}</option>
                                </select>
                            </div>
                            <div class="form-group mt-2">
                                <label>Tình Trạng</label>
                                <select class="form-control" v-model="add.is_open" name="is_open">
                                    <option value="0">Hiển Thị</option>
                                    <option value="1">Tạm Khóa</option>
                                </select>
                            </div>
                            <button type="submit" v-on:click="createDanhmuc($event)" class="btn btn-primary mt-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                       <h3> Thêm mới danh mục quần áo</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Mã Danh Mục</th>
                                    <th class="text-center">Tên Danh Mục</th>
                                    <th class="text-center">Danh Mục Cha</th>
                                    <th class="text-center">Tình Trạng</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <template v-for="(value, key) in table">
                                    <tr>
                                        <th class="text-center text-nowrap">@{{key + 1}}</th>
                                        <td class="text-center text-nowrap">@{{value.ma_danh_muc}}</td>
                                        <td class="text-center text-nowrap">@{{value.ten_danh_muc}}</td>
                                        <td v-if="value.id_danh_muc_cha == 0" class="text-center text-nowrap">Root</td>
                                        <td v-else class="text-center text-nowrap">@{{value.ten_danh_muc_cha}}</td>
                                        <td class="text-center">
                                            <button class="btn btn-primary" v-on:click="changeStatus(value.id)" v-if="value.is_open == 0">Hiển Thị</button>
                                            <button class="btn btn-danger"  v-on:click="changeStatus(value.id)" v-else>Tạm Khóa</button>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-danger" v-on:click="deleteDanhmuc(value.id)">Xóa</button>
                                        </td>
                                    </tr>
                                </template>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script>
        new Vue({
            el: '#app',
            data: {
                add     : {},
                edit    : {},
                table   :[],
                listDanhMucCha : [],
            },
            created() {
                this.loadTable();
            },
            methods: {
                createDanhmuc(e){
                    e.preventDefault();
                    axios
                        .post('/admin/danh-muc/create', this.add)
                        .then((res) => {
                            if(res.data.status){
                                this.loadTable();
                                toastr.success('Đã Thêm Mới Danh Mục Thành Công');
                            }
                        })
                        .catch((res) => {
                            // console.log(res);
                            var listError = res.response.data.errors;
                            $.each(listError, function(key, value) {
                                toastr.error(value[0]);
                            });
                        });
                },

                loadTable(){
                    axios
                        .get('/admin/danh-muc/get-data')
                        .then((res) => {
                            this.table = res.data.data;
                            this.listDanhMucCha = res.data.listDanhMucCha;
                        });
                },
                converToSlug(str) {
                    str = str.toLowerCase();

                    str = str
                        .normalize('NFD') // chuyển chuỗi sang unicode tổ hợp
                        .replace(/[\u0300-\u036f]/g, ''); // xóa các ký tự dấu sau khi tách tổ hợp

                    // Thay ký tự đĐ
                    str = str.replace(/[đĐ]/g, 'd');

                    // Xóa ký tự đặc biệt
                    str = str.replace(/([^0-9a-z-\s])/g, '');

                    // Xóa khoảng trắng thay bằng ký tự -
                    str = str.replace(/(\s+)/g, '-');

                    // Xóa ký tự - liên tiếp
                    str = str.replace(/-+/g, '-');

                    // xóa phần dư - ở đầu & cuối
                    str = str.replace(/^-+|-+$/g, '');

                    // return
                    this.add.slug_danh_muc = str;
                },

                changeStatus($id){
                    console.log('đã vào đây');
                    axios
                        .get('/admin/danh-muc/change-status/'+ $id)
                        .then((res) => {
                            if(res.data.status){
                                toastr.success('Đã đổi trạng thái !!!');
                                this.loadTable();
                            } else {
                                toastr.error('có lỗi !!!');
                            }
                        });
                },

                deleteDanhmuc($id){
                    axios
                        .get('/admin/danh-muc/delete/'+ $id)
                        .then((res) => {
                            if(res.data.status){
                                toastr.success('Đã xóa !!!');
                                this.loadTable();
                            } else {
                                toastr.error('có lỗi !!!');
                            }
                        });
                },
            }
        })
    </script>
@endsection
