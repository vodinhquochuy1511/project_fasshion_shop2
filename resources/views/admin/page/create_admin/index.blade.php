@extends('admin.master')
@section('content')
<div id="app" class="page-body">
    <div class="container-fuild">
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h3>Thêm mới tài khoản</h3>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                              <label>Họ và tên</label>
                              <input type="text" v-model="add.full_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" v-model="add.email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại</label>
                                <input type="number" v-model="add.phone" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>Mật khẩu</label>
                              <input type="password" v-model="add.password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Nhập lại mật khẩu</label>
                                <input type="password" v-model="add.re_password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tình Trạng</label>
                                <select class="form-control mb-3" v-model="add.is_open">
                                    <option value="0">Hiển Thị</option>
                                    <option value="1">Đã Khóa</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" v-on:click="createAdmin($event)">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                         <h3>Thông Tin Tài Khoản</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Tên</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Số Điện Thoại</th>
                                    <th class="text-center">Trạng Thái</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(value, key) in listAdmin">
                                    <th class="text-center">@{{ key + 1}}</th>
                                    <td class="text-center">@{{value.full_name}}</td>
                                    <td class="text-center text-nowrap">@{{value.email}}</td>
                                    <td class="text-center text-nowrap">@{{value.phone}}</td>
                                    <td class="text-center">
                                        <button class="btn btn-primary" v-if="value.is_open == 0" v-on:click="changStatus(value.id)">Hiển Thị</button>
                                        <button class="btn btn-danger"  v-on:click="changStatus(value.id)" v-else>Đã Khóa</button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-danger" v-on:click="deleteAdmin(value.id)">Xóa</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script>
         new Vue({
            el: '#app',
            data: {
                add     : {},
                listAdmin : [],
            },
            created() {
                this.loadtable()
            },
            methods: {
                createAdmin(e){
                    console.log(1);
                    e.preventDefault();
                    axios
                        .post('/admin/create-admin/create', this.add)
                        .then((res) => {
                            if(res.data.status){
                                toastr.success('Đã Thêm Mới Admin Thành Công');
                                this.loadtable()
                            }
                        })
                        .catch((res) => {
                            // console.log(res);
                            var listError = res.response.data.errors;
                            $.each(listError, function(key, value) {
                                toastr.error(value[0]);
                            });
                        });
                },

                loadtable(){
                    axios
                        .get('/admin/create-admin/get-data')
                        .then((res) => {
                            this.listAdmin = res.data.listAdmin;
                        });
                },
                changStatus(id){
                    axios
                        .get('/admin/create-admin/change-status' + id)
                        .then((res) => {
                            if(res.data.status){
                                toastr.success('Đã Đổi Trạng Thái Thành Công');
                                this.loadtable();
                            }
                        });
                },

                deleteAdmin(id){
                    axios
                        .get('/admin/create-admin/delete/' + id)
                        .then((res) => {
                            if(res.data.status){
                                toastr.success('Đã Đổi Xóa Admin');
                                this.loadtable();
                            }
                        });
                },

            },
        })
    </script>
@endsection
