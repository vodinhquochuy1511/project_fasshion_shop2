@extends('admin.master')
@section('content')
    <div id="app" class="page-body">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h2>Thêm mới sản phẩm</h2>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-group mt-3">
                                <label>Tên Sản Phẩm</label>
                                <input type="text" v-on:keyup="converToSlug(add.ten_san_pham)" v-model="add.ten_san_pham" class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Slug Sản Phẩm</label>
                                <input type="text" v-model="slug"class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Số lượng</label>
                                <input type="number" v-model="add.so_luong" class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Giá Bán</label>
                                <input type="number" v-model="add.gia_ban" class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Phần % giảm giá</label>
                                <input type="number" v-model="add.phan_tram_khuyen_mai" class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Hình Ảnh</label>
                                <div class="input-group">
                                    <input type="text" id="thumbnail" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text lfm bg-danger lfm" data-input="thumbnail" data-preview="holder">Choose</span>
                                     </div>
                                 </div>
                                 <div id="holder" style="margin-top:15px;max-height:100px;"></div>
                            </div>
                            <div class="form-group mt-3">
                                <label>Danh Mục Sản Phẩm</label>
                                <select class="form-control" v-model="add.id_danh_muc">
                                    <template v-for="(value, key) in listDanhMuc">
                                        <option class="form-control" v-bind:value="value.id">@{{value.ten_danh_muc}}</option>
                                    </template>
                                </select>
                            </div>
                            <div class="form-group mt-3">
                                <label>Tình Trạng</label>
                                <select class="form-control" v-model="add.is_open">
                                    <option value="0">Hiển Thị</option>
                                    <option value="1">Tạm Tắt</option>
                                </select>
                            </div>
                            {{-- <div class="form-group mt-3">
                                <label>Format Slug</label>
                                <input type="text" v-on:keyup="testt(test)" v-model="test" class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Kết quả slug</label>
                                <input type="text" v-model="kqTest" class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Kiểm tra gmail</label>
                                <input type="text" v-model="test2" class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Kiểm tra số điện thoại</label>
                                <input type="text" v-model="test3" class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Kết quả email</label>
                                <input type="text" v-model="kqTest2" class="form-control">
                            </div>
                            <div class="form-group mt-3">
                                <label>Kết quả số điện thoại</label>
                                <input type="text" v-model="kqTest3" class="form-control">
                            </div>
                            <button type="submit" v-on:click="ktTest(test2,$event)" class="btn btn-primary mt-3">KtrGmail</button>
                            <button type="submit" v-on:click="ktTestSdt(test3,$event)" class="btn btn-primary mt-3">KtrSĐT</button> --}}
                            <button type="submit" v-on:click="addSanpham($event)" class="btn btn-primary mt-3">Thêm Mới</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h2>Thông Tin Sản Phẩm</h2>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="text-nowrap">
                                        <th class="text-center">#</th>
                                        <th class="text-center">Mã Sản Phẩm</th>
                                        <th class="text-center">Tên Sản Phẩm</th>
                                        <th class="text-center">Hình Ảnh</th>
                                        <th class="text-center">Giá Bán</th>
                                        <th class="text-center">Giá Khuyến Mãi</th>
                                        <th class="text-center">Số Lượng</th>
                                        <th class="text-center">Loại Sản Phẩm</th>
                                        <th class="text-center">Tình Trạng</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <template v-for="(value, key) in listSanPham">
                                        <tr class="text-nowrap">
                                            <th class="text-center">@{{ key + 1}}</th>
                                            <td class="text-center">@{{ value.ma_san_pham}}</td>
                                            <td class="text-center">@{{ value.ten_san_pham}}</td>
                                            <td class="text-center">
                                                <img class="img-thumbnail" v-bind:src="value.hinh_anh">
                                            </td>
                                            <td class="text-center">@{{ format(value.gia_ban)}}</td>
                                            <td class="text-center">@{{ format(value.gia_khuyen_mai)}} <span>(@{{ value.phan_tram_khuyen_mai}}%)</span> </td>
                                            <td class="text-center">@{{ value.so_luong}}</td>
                                            <td class="text-center">@{{ value.ten_danh_muc}}</td>
                                            <td class="text-center">
                                                <button class="btn btn-primary" v-on:click="changeStatus(value.id)" v-if="value.is_open == 0">Hiển Thị</button>
                                                <button class="btn btn-danger" v-on:click="changeStatus(value.id)" v-else>Tạm Tắt</button>
                                            </td>
                                            <td class="text-center">
                                                <button class="btn btn-info" v-on:click="edit=value" data-bs-toggle="modal" data-bs-target=".clickEdit">Edit</button>
                                                <button class="btn btn-danger" v-on:click="deleteSanPham(value.id)">Xóa</button>
                                            </td>
                                        </tr>
                                    </template>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade clickEdit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-info">
                <h4 class="modal-title" id="myLargeModalLabel">Cập Nhập Sản Phẩm</h4>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="text" v-model="edit.id" hidden>
                        <div class="form-group mt-3">
                            <label>Tên Sản Phẩm</label>
                            <input type="text" v-on:keyup="converToSlug(edit.ten_san_pham)" v-model="edit.ten_san_pham" class="form-control">
                        </div>
                        <div class="form-group mt-3">
                            <label>Slug Sản Phẩm</label>
                            <input type="text" v-model="edit.slug_san_pham"class="form-control">
                        </div>
                        <div class="form-group mt-3">
                            <label>Số lượng</label>
                            <input type="number" v-model="edit.so_luong" class="form-control">
                        </div>
                        <div class="form-group mt-3">
                            <label>Giá Bán</label>
                            <input type="number" v-model="edit.gia_ban" class="form-control">
                        </div>
                        <div class="form-group mt-3">
                            <label>Phần % giảm giá</label>
                            <input type="number" v-model="edit.phan_tram_khuyen_mai" class="form-control">
                        </div>
                        <div class="form-group mt-3">
                            <label>Hình Ảnh</label>
                            <input type="text" v-model="edit.hinh_anh" class="form-control">
                        </div>
                        <div class="form-group mt-3">
                            <label>Danh Mục Sản Phẩm</label>
                            <select class="form-control" v-model="edit.id_danh_muc">
                                <template v-for="(value, key) in listDanhMuc">
                                    <option class="form-control" v-bind:value="value.id">@{{value.ten_danh_muc}}</option>
                                </template>
                            </select>
                        </div>
                        <div class="form-group mt-3">
                            <label>Tình Trạng</label>
                            <select class="form-control" v-model="edit.is_open">
                                <option value="0">Hiển Thị</option>
                                <option value="1">Tạm Tắt</option>
                            </select>
                        </div>
                        <button type="submit" v-on:click="updateSanpham($event)" data-bs-dismiss="modal" class="btn btn-primary mt-3 pull-right ">Cập Nhập</button>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    new Vue({
        el :  '#app',
        data : {
            add : {},
            listSanPham : [],
            listDanhMuc : [],
            edit : {},
            test : '',
            test2 : '',
            test3 : '',
            kqTest : '',
            kqTest2 : '',
            kqTest3 : '',
            slug : '',
        },

        created() {
            this.loadTable();
            this.testtt();
        },

        methods : {

            addSanpham(e){
                e.preventDefault();
                this.add.hinh_anh = $('#thumbnail').val();
                this.add.slug_san_pham = this.slug;
                axios
                    .post('/admin/san-pham', this.add)
                    .then((res) => {
                        if(res.data.status){
                            toastr.success('Đã Thêm Mới Sản Phẩm Thành Công');
                            this.loadTable();
                        }
                    })
                    .catch((res) => {
                        var listError = res.response.data.errors;
                        $.each(listError, function(key, value) {
                            toastr.error(value[0]);
                        });
                    });
            },
            converToSlug(str) {
                    str = str.toLowerCase();

                    str = str
                        .normalize('NFD') // chuyển chuỗi sang unicode tổ hợp
                        .replace(/[\u0300-\u036f]/g, ''); // xóa các ký tự dấu sau khi tách tổ hợp

                    // Thay ký tự đĐ
                    str = str.replace(/[đĐ]/g, 'd');

                    // Xóa ký tự đặc biệt
                    str = str.replace(/([^0-9a-z-\s])/g, '');

                    // Xóa khoảng trắng thay bằng ký tự -
                    str = str.replace(/(\s+)/g, '-');

                    // Xóa ký tự - liên tiếp
                    str = str.replace(/-+/g, '-');

                    // xóa phần dư - ở đầu & cuối
                    str = str.replace(/^-+|-+$/g, '');

                    // return
                    this.slug = str;
            },
            loadTable(){
                axios
                    .get('/admin/san-pham/get-data')
                    .then((res) => {
                        this.listSanPham = res.data.listSanPham;
                        this.listDanhMuc = res.data.danhMuc;
                    });
            },
            changeStatus(id){
                axios
                    .get('/admin/san-pham/change-status/'+ id)
                    .then((res) => {
                        if(res.data.status){
                            toastr.success('Đã đổi trạng thái !!!');
                            this.loadTable();
                        } else {
                            toastr.error('có lỗi !!!');
                        }
                    });
            },
            deleteSanPham(id){
                axios
                    .get('/admin/san-pham/delete/'+ id)
                    .then((res) => {
                        if(res.data.status){
                            toastr.success('Đã xóa sản phẩm !!!');
                            this.loadTable();
                        } else {
                            toastr.error('có lỗi !!!');
                        }
                    });
            },
            updateSanpham(e){
                e.preventDefault();
                    axios
                        .post('/admin/san-pham/update', this.edit)
                        .then((res) => {
                            if(res.data.status){
                                toastr.success('Đã Cập Nhập Sản Phẩm Thành Công');
                                this.loadTable();
                            }
                        })
                        .catch((res) => {
                            var listError = res.response.data.errors;
                            $.each(listError, function(key, value) {
                                toastr.error(value[0]);
                            });
                        });
            },
            format(number) {
                return new Intl.NumberFormat("vi-VI",{ style: "currency", currency: "VND" }).format(number);
            },
            // testt(test){
            //     // test = test.replace(/[. + \s]/g,'')
            //     test = test.toLowerCase();
            //     test = test
            //         .normalize('NFD')
            //         .replace(/[\u0300-\u036f]/g, '');
            //     test = test.replace(/[đĐ]/g, 'd');
            //     test = test.replace(/(\s+)/g, '-');
            //     this.kqTest = test;
            // },

            // ktTest(number,e){
            //     e.preventDefault();

            //     const numberRegex = /[a-zA-Z0-9._-]+@gmail+\.com/;
            //     if(numberRegex.test(number)){
            //        return this.kqTest2 = number;
            //     }else{
            //        return this.kqTest2 = 'Gmail Sai Định Dạng';
            //     }
            // },
            // ktTestSdt(number,e){
            //     e.preventDefault();
            //     const numberRegex = /0[0-9]{9}/;
            //     if(numberRegex.test(number)){
            //        return this.kqTest3 = number;
            //     }else{
            //        return this.kqTest3 = 'Số điện thoại phải 10 số';
            //     }
            // },

        }
    })
</script>
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    $('.lfm').filemanager('image', {prefix: '/laravel-filemanager'});
    var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
};
</script>
@endsection
