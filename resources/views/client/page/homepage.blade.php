@extends('client.master')
@section('content')
    @include('client.compoment.slide');
    <section class="shop-section">
        <div class="auto-container">
            <div class="sec-title">
                <h2>Our Top Collection</h2>
                <p>There are some product that we featured for choose your best</p>
                <span class="separator" style="background-image: url(assets/images/icons/separator-1.png);"></span>
            </div>
            <div class="sortable-masonry">
                <div class="filters">
                    <ul class="filter-tabs filter-btns centred clearfix">
                        <li class="filter active" data-role="button" data-filter=".best_seller">Best Seller</li>
                        <li class="filter" data-role="button" data-filter=".new_arraivals">New Arraivals</li>
                        <li class="filter" data-role="button" data-filter=".top_rate">Top Rate</li>
                    </ul>
                </div>
                <div class="items-container row clearfix" style="position: relative; height: 913.912px;">
                    <div class="col-lg-3 col-md-6 col-sm-12 shop-block masonry-item small-column best_seller new_arraivals top_rate" style="position: absolute; left: 0px; top: 0px;">
                        <div class="shop-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <img src="assets/images/resource/shop/shop-1.jpg" alt="">
                                    <ul class="info-list clearfix">
                                        <li><a href="index.html"><i class="flaticon-heart"></i></a></li>
                                        <li><a href="product-details.html">Add to cart</a></li>
                                        <li><a href="assets/images/resource/shop/shop-1.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-search"></i></a></li>
                                    </ul>
                                </figure>
                                <div class="lower-content">
                                    <a href="product-details.html">Cold Crewneck Sweater</a>
                                    <span class="price">$70.30</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 shop-block masonry-item small-column best_seller" style="position: absolute; left: 300px; top: 0px;">
                        <div class="shop-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <img src="assets/images/resource/shop/shop-2.jpg" alt="">
                                    <span class="category green-bg">New</span>
                                    <ul class="info-list clearfix">
                                        <li><a href="index.html"><i class="flaticon-heart"></i></a></li>
                                        <li><a href="product-details.html">Add to cart</a></li>
                                        <li><a href="assets/images/resource/shop/shop-2.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-search"></i></a></li>
                                    </ul>
                                </figure>
                                <div class="lower-content">
                                    <a href="product-details.html">Multi-Way Ultra Crop Top</a>
                                    <span class="price">$50.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 shop-block masonry-item small-column best_seller top_rate" style="position: absolute; left: 600px; top: 0px;">
                        <div class="shop-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <img src="assets/images/resource/shop/shop-3.jpg" alt="">
                                    <ul class="info-list clearfix">
                                        <li><a href="index.html"><i class="flaticon-heart"></i></a></li>
                                        <li><a href="product-details.html">Add to cart</a></li>
                                        <li><a href="assets/images/resource/shop/shop-3.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-search"></i></a></li>
                                    </ul>
                                </figure>
                                <div class="lower-content">
                                    <a href="product-details.html">Side-Tie Tank</a>
                                    <span class="price">$40.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 shop-block masonry-item small-column best_seller new_arraivals" style="position: absolute; left: 900px; top: 0px;">
                        <div class="shop-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <img src="assets/images/resource/shop/shop-4.jpg" alt="">
                                    <ul class="info-list clearfix">
                                        <li><a href="index.html"><i class="flaticon-heart"></i></a></li>
                                        <li><a href="product-details.html">Add to cart</a></li>
                                        <li><a href="assets/images/resource/shop/shop-4.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-search"></i></a></li>
                                    </ul>
                                </figure>
                                <div class="lower-content">
                                    <a href="product-details.html">Cold Crewneck Sweater</a>
                                    <span class="price">$60.30</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 shop-block masonry-item small-column best_seller top_rate" style="position: absolute; left: 0px; top: 456px;">
                        <div class="shop-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <img src="assets/images/resource/shop/shop-5.jpg" alt="">
                                    <ul class="info-list clearfix">
                                        <li><a href="index.html"><i class="flaticon-heart"></i></a></li>
                                        <li><a href="product-details.html">Add to cart</a></li>
                                        <li><a href="assets/images/resource/shop/shop-5.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-search"></i></a></li>
                                    </ul>
                                </figure>
                                <div class="lower-content">
                                    <a href="product-details.html">Side-Tie Tank</a>
                                    <span class="price">$35.30</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 shop-block masonry-item small-column best_seller new_arraivals" style="position: absolute; left: 300px; top: 456px;">
                        <div class="shop-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <img src="assets/images/resource/shop/shop-6.jpg" alt="">
                                    <ul class="info-list clearfix">
                                        <li><a href="index.html"><i class="flaticon-heart"></i></a></li>
                                        <li><a href="product-details.html">Add to cart</a></li>
                                        <li><a href="assets/images/resource/shop/shop-6.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-search"></i></a></li>
                                    </ul>
                                </figure>
                                <div class="lower-content">
                                    <a href="product-details.html">Must-Have Easy Tank</a>
                                    <span class="price">$25.30</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 shop-block masonry-item small-column best_seller new_arraivals top_rate" style="position: absolute; left: 600px; top: 456px;">
                        <div class="shop-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <img src="assets/images/resource/shop/shop-7.jpg" alt="">
                                    <span class="category red-bg">Hot</span>
                                    <ul class="info-list clearfix">
                                        <li><a href="index.html"><i class="flaticon-heart"></i></a></li>
                                        <li><a href="product-details.html">Add to cart</a></li>
                                        <li><a href="assets/images/resource/shop/shop-7.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-search"></i></a></li>
                                    </ul>
                                </figure>
                                <div class="lower-content">
                                    <a href="product-details.html">Woven Crop Cami</a>
                                    <span class="price">$90.30</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 shop-block masonry-item small-column best_seller new_arraivals" style="position: absolute; left: 900px; top: 456px;">
                        <div class="shop-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <img src="assets/images/resource/shop/shop-8.jpg" alt="">
                                    <ul class="info-list clearfix">
                                        <li><a href="index.html"><i class="flaticon-heart"></i></a></li>
                                        <li><a href="product-details.html">Add to cart</a></li>
                                        <li><a href="assets/images/resource/shop/shop-8.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-search"></i></a></li>
                                    </ul>
                                </figure>
                                <div class="lower-content">
                                    <a href="product-details.html">Must-Have Easy Tank</a>
                                    <span class="price">$20.30</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="more-btn centred"><a href="shop.html" class="theme-btn-one">View All Products<i class="flaticon-right-1"></i></a></div>
        </div>
    </section>
@endsection
@section('js')

@endsection
