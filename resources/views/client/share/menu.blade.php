<header class="main-header">
    <div class="header-top">
        <div class="auto-container">
            <div class="top-inner clearfix">
                <div class="top-left pull-left">
                    <ul class="info clearfix">
                        <li><i class="flaticon-email"></i><a href="mailto:support@example.com">support@example.com</a></li>
                        <li><i class="flaticon-global"></i> Kleine Pierbard 8-6 2249 KV Vries</li>
                    </ul>
                </div>
                <div class="top-right pull-right">
                    <ul class="social-links clearfix">
                        <li><a href="index.html"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="index.html"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="index.html"><i class="fab fa-vimeo-v"></i></a></li>
                        <li><a href="index.html"><i class="fab fa-google-plus-g"></i></a></li>
                    </ul>
                    <div class="language">
                        <div class="lang-btn">
                            <span class="flag"><img src="assets/images/icons/icon-lang.png" alt="" title="English"></span>
                            <span class="txt">English</span>
                            <span class="arrow fa fa-angle-down"></span>
                        </div>
                        <div class="lang-dropdown">
                            <ul>
                                <li><a href="index.html">German</a></li>
                                <li><a href="index.html">Italian</a></li>
                                <li><a href="index.html">Chinese</a></li>
                                <li><a href="index.html">Russian</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="price-box">
                        <span>USD</span>
                        <ul class="price-list clearfix">
                            <li><a href="index.html">USD</a></li>
                            <li><a href="index.html">UK</a></li>
                            <li><a href="index.html">URO</a></li>
                            <li><a href="index.html">Spanish</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-lower">
        <div class="auto-container">
            <div class="outer-box">
                <figure class="logo-box"><a href="index.html"><img src="assets/images/logo.png" alt=""></a></figure>
                <div class="menu-area">
                    <!--Mobile Navigation Toggler-->
                    <div class="mobile-nav-toggler">
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                    </div>
                    <nav class="main-menu navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                            <ul class="navigation clearfix">
                                @foreach ($danhMucCha as $key => $value )
                                <li class="dropdown"><a href="index.html">{{$value->ten_danh_muc}}</a>
                                    <ul>
                                        @foreach ($danhMucCon as $k => $v )
                                        @if ($value->id == $v->id_danh_muc_cha)
                                        <li><a href="about.html">{{$v->ten_danh_muc}}</a></li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <ul class="menu-right-content clearfix">
                    <li>
                        <div class="search-btn">
                            <button type="button" class="search-toggler"><i class="flaticon-search"></i></button>
                        </div>
                    </li>
                    <li><a href="index.html"><i class="flaticon-like"></i></a></li>
                    <li><a href="index.html"><i class="flaticon-user"></i></a></li>
                    <li class="shop-cart">
                        <a href="shop.html"><i class="flaticon-shopping-cart-1"></i><span>3</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--sticky Header-->
    <div class="sticky-header">
        <div class="auto-container">
            <div class="outer-box clearfix">
                <div class="logo-box pull-left">
                    <figure class="logo"><a href="index.html"><img src="assets/images/small-logo.png" alt=""></a></figure>
                </div>
                <div class="menu-area pull-right">
                    <nav class="main-menu clearfix">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
 <!-- Mobile Menu  -->
 <div class="mobile-menu">
    <div class="menu-backdrop"></div>
    <div class="close-btn"><i class="fas fa-times"></i></div>
    <nav class="menu-box">
        <div class="nav-logo"><a href="index.html"><img src="assets/images/logo-2.png" alt="" title=""></a></div>
        <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
        <div class="contact-info">
            <h4>Contact Info</h4>
            <ul>
                <li>Chicago 12, Melborne City, USA</li>
                <li><a href="tel:+8801682648101">+88 01682648101</a></li>
                <li><a href="mailto:info@example.com">info@example.com</a></li>
            </ul>
        </div>
        <div class="social-links">
            <ul class="clearfix">
                <li><a href="index.html"><span class="fab fa-twitter"></span></a></li>
                <li><a href="index.html"><span class="fab fa-facebook-square"></span></a></li>
                <li><a href="index.html"><span class="fab fa-pinterest-p"></span></a></li>
                <li><a href="index.html"><span class="fab fa-instagram"></span></a></li>
                <li><a href="index.html"><span class="fab fa-youtube"></span></a></li>
            </ul>
        </div>
    </nav>
</div><!-- End Mobile Menu -->
