<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test', [\App\Http\Controllers\TestController::class, 'index']);

Route::get('/', [\App\Http\Controllers\HomepageController::class, 'index']);
Route::get('/admin/login', [\App\Http\Controllers\AdminController::class, 'viewLogin']);


Route::group(['prefix' => '/admin'], function() {
    Route::group(['prefix' => '/danh-muc'], function() {
        Route::get('/index', [\App\Http\Controllers\DanhMucController::class, 'index']);
        Route::post('/create', [\App\Http\Controllers\DanhMucController::class, 'Create']);
        Route::get('/get-data', [\App\Http\Controllers\DanhMucController::class, 'getData']);
        Route::get('/change-status/{id}', [\App\Http\Controllers\DanhMucController::class, 'changeStatus']);
        Route::get('/delete/{id}', [\App\Http\Controllers\DanhMucController::class, 'Delete']);
    });
    Route::group(['prefix' => '/san-pham'], function() {
        Route::get('/index', [\App\Http\Controllers\SanPhamController::class, 'index']);
        Route::post('/', [\App\Http\Controllers\SanPhamController::class, 'CreateSanPham']);
        Route::get('/get-data', [\App\Http\Controllers\SanPhamController::class, 'getData']);
        Route::get('/change-status/{id}', [\App\Http\Controllers\SanPhamController::class, 'changeStatus']);
        Route::get('/delete/{id}', [\App\Http\Controllers\SanPhamController::class, 'deleteSanPham']);
        Route::post('/update', [\App\Http\Controllers\SanPhamController::class, 'UpdateSanPham']);
    });
    Route::group(['prefix' => '/create-admin'], function() {
        Route::get('/index', [\App\Http\Controllers\AdminController::class, 'index']);
        Route::post('/create', [\App\Http\Controllers\AdminController::class, 'createAdmin']);
        Route::get('/get-data', [\App\Http\Controllers\AdminController::class, 'getData']);
        Route::get('/change-status{id}', [\App\Http\Controllers\AdminController::class, 'changeStatus']);
        Route::get('/delete/{id}', [\App\Http\Controllers\AdminController::class, 'deleteAdmin']);

    });

});
Route::group(['prefix' => 'laravel-filemanager'], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});


