<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSanPhanRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'ten_san_pham'              =>   'required|min:3',
            'slug_san_pham'             =>   'required',
            'so_luong'                  =>   'required|min:1',
            'gia_ban'                   =>   'required|numeric',
            'hinh_anh'                  =>   'required',
            'phan_tram_khuyen_mai'      =>   'nullable|numeric|max:100',
            'so_luong'                  =>   'required|min:1',
            'id_danh_muc'               =>   'required',
            'is_open'                   =>   'required|boolean',
        ];
    }
}
