<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdminRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
        'full_name'         => 'required|min:3',
        'email'             => 'required|email|unique:admins,email',
        'phone'             => 'required|numeric|digits:10',
        'password'          => 'required|min:5',
        're_password'       => 'required|same:password',
        'is_open'           => 'required|boolean',
        ];
    }
}
