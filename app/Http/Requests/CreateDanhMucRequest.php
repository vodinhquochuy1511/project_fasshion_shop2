<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDanhMucRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
           'ma_danh_muc'            => 'required|min:3|unique:danh_mucs,ma_danh_muc',
           'ten_danh_muc'           => 'required|min:3',
           'slug_danh_muc'          => 'required|min:3',
           'id_danh_muc_cha'        => 'required',
           'is_open'                => 'required|boolean',
        ];
    }
}
