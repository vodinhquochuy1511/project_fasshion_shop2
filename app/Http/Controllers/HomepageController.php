<?php

namespace App\Http\Controllers;

use App\Models\DanhMuc;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index()
    {
        return view('client.page.homepage');
    }
}
