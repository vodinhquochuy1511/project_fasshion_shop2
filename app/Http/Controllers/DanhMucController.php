<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDanhMucRequest;
use App\Models\DanhMuc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yoeunes\Toastr\Facades\Toastr;

class DanhMucController extends Controller
{
    public function index()
    {
        return view('admin.page.danh_muc.index');
    }

    public function Create(CreateDanhMucRequest $request)
    {
        $data = $request->all();

        DanhMuc::create($data);
        return response()->json([
            'status'   =>   true,
        ]);
    }

    public function getData()
    {
        $listDanhMucCha = DanhMuc::where('id_danh_muc_cha', 0)->get();
        $sql = "SELECT A.ten_danh_muc AS ten_danh_muc_cha, B.*  FROM danh_mucs A RIGHT JOIN danh_mucs B ON A.id = B.id_danh_muc_cha";
        $list = DB::select($sql);
        return response()->json(['data' => $list, 'listDanhMucCha' =>  $listDanhMucCha ]);
    }

    public function changeStatus($id)
    {
        $data = DanhMuc::find($id);

        if($data){
            $data->is_open = !$data->is_open;
            $data->save();
            return response()->json([
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false
            ]);
        }
    }

    public function Delete($id)
    {
        $data = DanhMuc::find($id);

        if($data){
            $data->delete();
            return response()->json([
                'status' => true,
            ]);
        } else {
            return response()->json([
                'status' => false,
            ]);
        }
    }
}
