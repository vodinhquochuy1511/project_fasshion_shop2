<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSanPhanRequest;
use App\Http\Requests\UpdateSanPhamRequest;
use App\Models\DanhMuc;
use App\Models\SanPham;
use Illuminate\Http\Request;

class SanPhamController extends Controller
{

    public function index()
    {
        return view('admin.page.san_pham.index');
    }

    public function CreateSanPham(CreateSanPhanRequest $request)
    {
        $data = $request->all();
        if($request->phan_tram_khuyen_mai){
            $data['gia_khuyen_mai'] = $request->gia_ban - ($request->phan_tram_khuyen_mai  / 100 * $request->gia_ban) ;
        }else{
            $data['gia_khuyen_mai'] = 0;
        }
        $ma_san_pham = SanPham::latest()->first();
        if ($ma_san_pham) {
            $data['ma_san_pham'] = 'SP'. (10000 + $ma_san_pham->id);
        } else {
            $data['ma_san_pham'] = 'SP10000';
        }

        SanPham::create($data);

        return response()->json([
            'status'  =>  true,
        ]);
    }

    public function getData()
    {
        $listSanPham = SanPham::join('danh_mucs','danh_mucs.id','san_phams.id_danh_muc')
                                ->select('danh_mucs.ten_danh_muc','danh_mucs.is_open', 'san_phams.*')
                                ->where('danh_mucs.is_open', 0)
                                ->get();
        $danhMuc = DanhMuc::where('id_danh_muc_cha', '<>', 0)->get();
        // dd($listSanPham->toArray());
        return response()->json([
            'listSanPham'  =>   $listSanPham,
            'danhMuc'      =>   $danhMuc,
        ]);
    }

    public function changeStatus($id)
    {
        $sanPham = SanPham::find($id);
        if($sanPham){
            $sanPham->is_open = !$sanPham->is_open;
            $sanPham->save();
            return response()->json([
                'status'    => true,
            ]);
        }else{
            return response()->json([
                'status'    => false,
            ]);
        }
    }

    public function deleteSanPham($id)
    {
        $sanPham = SanPham::find($id);

        if($sanPham){
            $sanPham->delete();
            return response()->json([
                'status'    => true,
            ]);
        }else{
            return response()->json([
                'status'    => false,
            ]);
        }
    }

    public function UpdateSanPham(UpdateSanPhamRequest $request)
    {
        $data = $request->all();

        $sanPham =  SanPham::find($request->id);

        if($request->phan_tram_khuyen_mai > 0){
            $data['gia_khuyen_mai'] = $request->gia_ban - ($request->phan_tram_khuyen_mai  / 100 * $request->gia_ban) ;
        }else{
            $data['gia_khuyen_mai'] = 0;
        }

        $sanPham->update($data);

        return response()->json([
            'status' => true,
        ]);
    }

}
