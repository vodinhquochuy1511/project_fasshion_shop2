<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdminRequest;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function viewLogin()
    {
        return view('admin.auth.index');
    }

    public function index()
    {
        return view('admin.page.create_admin.index');
    }

    public function createAdmin(CreateAdminRequest $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($request->password);

        Admin::create($data);

        return response()->json([
            'status' => true
        ]);
    }

    public function getData()
    {
        $listAdmin = Admin::all();

        return response()->json([
            'listAdmin' => $listAdmin,
        ]);
    }

    public function changeStatus($id)
    {
        $admin = Admin::find($id);

        if($admin){
            $admin->is_open = !$admin->is_open;
            $admin->save();
        }
        return response()->json([
            'status' => true,
        ]);
    }

    public function deleteAdmin($id)
    {
        $admin = Admin::find($id);

        if($admin){
            $admin->delete();
        }
        return response()->json([
            'status' => true,
        ]);
    }
}
