<?php

namespace App\Providers;

use App\Models\DanhMuc;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        //
    }


    public function boot()
    {
        $danhMucCha = DanhMuc::where('is_open', 0)
                            ->where('id_danh_muc_cha', 0)
                            ->get();
        $danhMucCon = DanhMuc::where('is_open', 0)
                            ->where('id_danh_muc_cha', '<>', 0)
                            ->get();
                            // dd($danhMucCha->toArray(),$danhMucCon->toArray());
        view()->share('danhMucCha', $danhMucCha);
        view()->share('danhMucCon', $danhMucCon);
    }
}
