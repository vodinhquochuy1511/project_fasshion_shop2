<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SanPham extends Model
{
    use HasFactory;

    protected $table = 'san_phams';

    protected $fillable = [
        'ma_san_pham',
        'ten_san_pham',
        'slug_san_pham',
        'so_luong',
        'id_danh_muc',
        'is_open',
        'gia_ban',
        'hinh_anh',
        'phan_tram_khuyen_mai',
        'gia_khuyen_mai',
    ];
}
